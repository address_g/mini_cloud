# 云开发 quickstart

这是云开发的快速启动指引，其中演示了如何上手使用云开发的三大基础能力：

- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码

# 云开发

- 在项目根目录下新建云函数文件夹（与小程序项目代码同级）
- 在"project.config.json"文件下添加云函数根目录  -->  "cloudfunctionRoot": "cloudfunctions/"

# 新建云函数

- 右键云函数根目录"cloudfunctions"  -->  新建 nodejs 云函数（以test命名为例）
- 右键test文件夹  -->  选择"在外部终端窗口中打开" -->  安装依赖(npm i)
- 右键test文件夹  -->  上传并部署：所有文件

# 修改云函数

- 修改index.js  -->  右键 index.js  -->  云函数增量上传：更新文件

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)