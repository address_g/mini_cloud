/**
 * 1、云数据库操作
 *  1.1 小程序api
 *    会有一套非常严格的调用权限控制，只能进行非敏感的数据操作（只能修改自己创建的数据；每次最多获取20条数据）
 * 
 *  1.2 云函数api
 *    所有敏感操作直接在云函数中处理（拥有所有数据的读写权限）
 */

const getStudents = require('./getStudents/index')
const getStudent = require('./getStudent/index')
const addStudent = require('./addStudent/index')
const delStudent = require('./delStudent/index')
const updStudent = require('./updStudent/index')

// 云函数入口函数
exports.main = async (event, context) => {
  switch (event.type) {
    case 'getStudents':
      return await getStudents.main(event, context)
    case 'getStudent':
      return await getStudent.main(event, context)
    case 'addStudent':
      return await addStudent.main(event, context)
    case 'delStudent':
      return await delStudent.main(event, context)
    case 'updStudent':
      return await updStudent.main(event, context)
  }
}