// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})

// 获取云数据库用例
const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection('student').add({
    data: event.student
  })
}